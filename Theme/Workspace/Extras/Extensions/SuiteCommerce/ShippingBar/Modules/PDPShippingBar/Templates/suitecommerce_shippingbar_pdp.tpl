{{#if freeShippingAvailable}}
{{else}}
    <section class="sc-shippingbar-pdp-message">
        {{#if showDetailBanner}}
            {{#if getFreeShippingWithProduct}}
                <p class="sc-shippingbar-pdp-qualifies-text">{{{itemQualifyMessage}}}</p>
            {{else}}
                <p class="sc-shippingbar-pdp-progress-text">{{{progressMessage}}}</p>
            {{/if}}
        {{/if}}
    </section>
{{/if}}