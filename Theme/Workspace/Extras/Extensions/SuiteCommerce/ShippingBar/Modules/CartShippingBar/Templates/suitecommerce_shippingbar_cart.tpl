<section data-view="Cart.Shipping.Bar" >
    {{#if showBanner}}
        <div class="sc-shippingbar-cart-container">
            {{#if shippingGoalAvailable}}
                <p class="sc-shippingbar-cart-message sc-shippingbar-cart-congratulations {{device}}">{{{cartCongratsMessage}}}</p>
            {{else}}
                {{#if isInProgress}}
                    <p class="sc-shippingbar-cart-message sc-shippingbar-cart-progress {{device}}">{{{cartProgressMessage}}}</p>
                {{else}}
                    <p class="sc-shippingbar-cart-message sc-shippingbar-cart-initial {{device}}">{{{cartInitialMessage}}}</p>
                {{/if}}
            {{/if}}
        </div>
    {{/if}}
</section>