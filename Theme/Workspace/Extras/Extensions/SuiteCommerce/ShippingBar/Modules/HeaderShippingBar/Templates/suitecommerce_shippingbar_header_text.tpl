{{#each messagesList}}
    {{#if isActive}}
    <div class="sc-shippingbar-header-text-transition-base sc-shippingbar-message shippingbar-text-{{@index}} sc-shippingbar-header-text-active">
        {{#if link}}
            <a href={{link}} data-action = "click-header-bar-message" target="_blank" class="sc-shippingbar-header-text-initial {{@index}} sc-shippingbar-header-text sc-shippingbar-text-message-ref-for-height">{{{message}}}</a>
        {{else}}
            <p class="sc-shippingbar-header-text-initial sc-shippingbar-header-text sc-shippingbar-text-message-ref-for-height">{{{message}}}</p>
        {{/if}}
    </div>
    {{else}}
    <div class="sc-shippingbar-header-text-transition-base sc-shippingbar-message shippingbar-text-{{@index}}">
        {{#if link}}
            <a href={{link}} data-action = "click-header-bar-message" target="_blank" class="sc-shippingbar-header-text-initial {{@index}} sc-shippingbar-header-text sc-shippingbar-text-message-ref-for-height">{{{message}}}</a>
        {{else}}
            <p class="sc-shippingbar-header-text-initial sc-shippingbar-header-text sc-shippingbar-text-message-ref-for-height">{{{message}}}</p>
        {{/if}}
    </div>
    {{/if}}
{{/each}}
