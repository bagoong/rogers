<section>
    {{#if shippingBarOpen}}
        {{#if isFloating}}
            <!--googleoff: index-->
            <!-- The entire bar code is repeated here in order to move down the website the exact height when the bar is set to float -->
            <div class="sc-shippingbar-header-background-transition-base sc-shippingbar-header-background sc-shippingbar-header-background{{#if shippingGoalAvailable}}-congratulations{{else if isInProgress}}-progress{{else}}-initial-{{currentMessageIndex}}{{/if}} sc-shippingbar-header-message-{{#if multipleMessages}}multi{{else}}single{{/if}}">
                <div class="sc-shippingbar-header">
                    <div class="sc-shippingbar-header-row-wrapper">
                        <div class="sc-shippingbar-header-container-row">
                            <div class="{{#if isFloating}}col-md-2 col-sm-2 col-xs-2{{else}}col-md-1 col-sm-1 col-xs-1{{/if}} sc-shippingbar-header-chevron-container">
                                {{#if multipleMessages}}
                                    <i class="sc-shippingbar-header-chevron-left" data-action="previous-text"></i>
                                {{/if}}
                            </div>
                            <div class="{{#if isFloating}}col-md-8 col-sm-8 col-xs-8{{else}}col-md-10 col-sm-10 col-xs-10{{/if}} sc-shippingbar-header-text-center">
                                {{#if shippingGoalAvailable}}
                                    {{#if haveLink}}
                                        <a a href={{urlLink}} target="_blank" class="sc-shippingbar-header-text-congratulations sc-shippingbar-text-message-ref-for-height">{{headerCongratsMessage}}</a>
                                    {{else}}
                                        <p class="sc-shippingbar-header-text-congratulations sc-shippingbar-text-message-ref-for-height">{{headerCongratsMessage}}</p>
                                    {{/if}}
                                {{else}}
                                    {{#if isInProgress}}
                                        {{#if haveLink}}
                                            <a href={{urlLink}} target="_blank" class="sc-shippingbar-header-text-progress sc-shippingbar-text-message-ref-for-height">{{{headerProgressMessage}}}</a>
                                        {{else}}
                                            <p class="sc-shippingbar-header-text-progress sc-shippingbar-text-message-ref-for-height">{{{headerProgressMessage}}}</p>
                                        {{/if}}

                                    {{else}}
                                        <div class="sc-shippingbar-header-textlist-wrapper">
                                            <div data-view="ShippingBar.Header.Text"></div>
                                        </div>
                                    {{/if}}
                                {{/if}}
                            </div>
                            <div class="col-md-1 col-sm-1 col-xs-1 sc-shippingbar-header-chevron-container">
                                {{#if multipleMessages}}
                                    <i class="sc-shippingbar-header-chevron-right" data-action = "next-text"></i>
                                {{/if}}
                            </div>
                            {{#if isFloating}}
                                <div class="col-md-1 col-sm-1 col-xs-1 sc-shippingbar-header-close-container">
                                    <i class="sc-shippingbar-header-times-circle" data-action="close-shipping-bar"></i>
                                </div>
                            {{/if}}
                        </div>
                    </div>
                </div>
            </div>
            <!--googleon: index-->
        {{/if}}


        <div class="sc-shippingbar-header-background-transition-base sc-shippingbar-header-background sc-shippingbar-header-background{{#if shippingGoalAvailable}}-congratulations{{else if isInProgress}}-progress{{else}}-initial-{{currentMessageIndex}}{{/if}} {{#if isFloating}}sc-shippingbar-header-floating{{/if}} sc-shippingbar-header-message-{{#if multipleMessages}}multi{{else}}single{{/if}}">
            <div class="sc-shippingbar-header">
                <div class="sc-shippingbar-header-row-wrapper">
                    <div class="sc-shippingbar-header-container-row">
                        <div class="{{#if isFloating}}col-md-2 col-sm-2 col-xs-2{{else}}col-md-1 col-sm-1 col-xs-1{{/if}} sc-shippingbar-header-chevron-container">
                            {{#if multipleMessages}}
                                <i class="sc-shippingbar-header-chevron-left" data-action="previous-text"></i>
                            {{/if}}
                        </div>
                        <div class="{{#if isFloating}}col-md-8 col-sm-8 col-xs-8{{else}}col-md-10 col-sm-10 col-xs-10{{/if}} sc-shippingbar-header-text-center">
                            {{#if shippingGoalAvailable}}
                                {{#if haveLink}}
                                    <a a href={{urlLink}} target="_blank" class="sc-shippingbar-header-text-congratulations sc-shippingbar-text-message-ref-for-height" id="header-congrats-message">{{{headerCongratsMessage}}}</a>
                                {{else}}
                                    <p class="sc-shippingbar-header-text-congratulations sc-shippingbar-text-message-ref-for-height" id="header-congrats-message">{{{headerCongratsMessage}}}</p>
                                {{/if}}

                            {{else}}
                                {{#if isInProgress}}
                                    {{#if haveLink}}
                                        <a href={{urlLink}} target="_blank" class="sc-shippingbar-header-text-progress sc-shippingbar-text-message-ref-for-height" id="header-progress-message">{{{headerProgressMessage}}}</a>
                                    {{else}}
                                        <p class="sc-shippingbar-header-text-progress sc-shippingbar-text-message-ref-for-height" id="header-progress-message">{{{headerProgressMessage}}}</p>
                                    {{/if}}
                                {{else}}
                                    <div class="sc-shippingbar-header-textlist-wrapper">
                                        <div data-view="ShippingBar.Header.Text"></div>
                                    </div>
                                {{/if}}
                            {{/if}}
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1 sc-shippingbar-header-chevron-container">
                            {{#if multipleMessages}}
                                <i class="sc-shippingbar-header-chevron-right" data-action = "next-text"></i>
                            {{/if}}
                        </div>
                        {{#if isFloating}}
                            <div class="col-md-1 col-sm-1 col-xs-1 sc-shippingbar-header-close-container">
                                <i class="sc-shippingbar-header-times-circle" data-action="close-shipping-bar"></i>
                            </div>
                        {{/if}}
                    </div>
                </div>
            </div>
        </div>
    {{/if}}
</section>