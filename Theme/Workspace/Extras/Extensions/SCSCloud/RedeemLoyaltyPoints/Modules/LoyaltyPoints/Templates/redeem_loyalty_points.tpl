<div class="order-wizard-loyaltypointsform">
		<div class="order-wizard-loyaltypointsform-expander-head">
			<a class="order-wizard-loyaltypointsform-expander-head-toggle collapsed" data-toggle="collapse" data-target="#order-wizard-loyaltypoints" aria-expanded="false" aria-controls="order-wizard-loyaltypoints">
				Loyalty Points
				<i class="order-wizard-loyaltypointsform-tooltip" data-toggle="tooltip" title="" data-original-title="<b>Loyalty Points</b><br>To redeem your loyalty points, simply enter your information and we will apply it to your purchase during checkout."></i>
				<i class="order-wizard-loyaltypointsform-expander-toggle-icon"></i>
			</a>
		</div>
		<div class="order-wizard-loyaltypointsform-expander-body collapse in" id="order-wizard-loyaltypoints" data-type="loyalty-points-container" data-action="show-loyalty-points-container" aria-expanded="false" data-target="#order-wizard-loyaltypoints">
			<div class="order-wizard-loyaltypointsform-expander-container">
				<p class="order-wizard-loyaltypointsform-available">Available: <strong>{{loyalty_points_balance}}</strong>{{#if pointsRedeemed}}<button title="Remove redeemed loyalty points." data-action="remove-loyalty-points">Remove points</button>{{/if}}</p>
				<form class="cart-loyaltypoints-form" data-action="apply-loyaltypoints">
					<div class="cart-loyaltypoints-form-summary-grid">
						<div class="cart-loyaltypoints-form-summary-container-input">
							<div class="">
								<input type="number" min="0" max="{{loyalty_points_balance_max}}" step="5" name="loyaltypoints" id="loyaltypoints" class="cart-loyaltypoints-form-summary-input" value="">
							</div>
						</div>
						<div class="cart-loyaltypoints-form-summary-loyaltypoints-container-button">
							{{#if showButton}}
							<button type="submit" class="cart-loyaltypoints-form-summary-button-apply-loyaltypoints" disabled="true">
								Redeem
							</button>
							{{/if}}
						</div>
						<div class="order-wizard-loyaltypoints-message"></div>
					</div>
					<div data-type="loyaltypoints-error-placeholder">
					</div>
				</form>
			</div>
		</div>
	</div>