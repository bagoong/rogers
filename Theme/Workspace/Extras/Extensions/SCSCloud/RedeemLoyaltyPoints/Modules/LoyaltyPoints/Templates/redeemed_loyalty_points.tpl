{{#if showLoyaltyPoints}}
<div class="order-wizard-cart-summary-points-redeemed-applied">  
    <a href="#" data-action="remove-points-redeemed" data-id="{{internalid}}" data-toggle="tooltip" title="Remove Points Redeemed.">
        <span class="points-redeemed-list-item-remove-action"><i></i></span>
    </a>
    <p class="order-wizard-cart-summary-grid-float">
        <span class="order-wizard-cart-summary-points-redeemed-formatted"> 
           - $10.00 
        </span> 
            {{translate 'Points redeemed'}}
    </p>
</div>
{{/if}}