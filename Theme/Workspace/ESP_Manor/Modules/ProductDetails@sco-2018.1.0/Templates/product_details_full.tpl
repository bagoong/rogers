{{!-- Edited for Manor Theme --}}

<div class="product-details-full">
	<div data-cms-area="item_details_banner" data-cms-area-filters="page_type"></div>

	<header class="product-details-full-header">
		<div id="banner-content-top" class="product-details-full-banner-top"></div>
	</header>

	<article class="product-details-full-content">

		<div id="banner-details-top" class="product-details-full-banner-top-details"></div>

		<section class="product-details-full-main-content">
			<div class="product-details-full-main-content-left">
				<div class="product-details-full-image-gallery-container">
					<div id="banner-image-top" class="content-banner banner-image-top"></div>
					<div data-view="Product.ImageGallery"></div>
					<div id="banner-image-bottom" class="content-banner banner-image-bottom"></div>

					<div data-cms-area="product_details_full_cms_area_2" data-cms-area-filters="path"></div>
					<div data-cms-area="product_details_full_cms_area_3" data-cms-area-filters="page_type"></div>
				</div>
			</div>

			<div class="product-details-full-main-content-right">
                <div class="product-details-full-main">
                    <div class="product-details-full-content-header">
                        <div data-cms-area="product_details_full_cms_area_1" data-cms-area-filters="page_type"></div>

                        <h1 class="product-details-full-content-header-title" itemprop="name">{{pageHeader}}</h1>
                        {{#unless isItemProperlyConfigured}}
                            <div class="product-details-full-rating" data-view="Global.StarRating"></div>
                        {{/unless}}
                        <div data-cms-area="item_info" data-cms-area-filters="path"></div>
                    </div>
                    {{#if isItemProperlyConfigured}}
                        <form id="product-details-full-form" data-action="submit-form" method="POST">

                            <section class="product-details-full-info">
                                <div id="banner-summary-bottom" class="product-details-full-banner-summary-bottom"></div>
                            </section>

                            <div data-view="Product.Sku"></div>

                            <div data-view="Product.Price"></div>
                            <div data-view="Quantity.Pricing"></div>

                            <div class="product-details-full-rating" data-view="Global.StarRating"></div>

                            <div data-view="Product.Stock.Info"></div>

                            <section data-view="Product.Options" class="product-details-full-product-options-container"></section>

                            <div data-cms-area="product_details_full_cms_area_4" data-cms-area-filters="path"></div>

                            {{#if isPriceEnabled}}
                                <div data-view="Quantity"></div>

                                <section class="product-details-full-actions">

                                    <div class="product-details-full-actions-container">
                                        <div data-view="MainActionView"></div>
                                        <div data-view="AddToProductList" class="product-details-full-actions-addtowishlist"></div>
                                    </div>
                                    <div class="product-details-full-actions-container">
                                        <div data-view="ProductDetails.AddToQuote" class="product-details-full-actions-addtoquote"></div>
                                    </div>

                                </section>
                            {{/if}}

                            <div data-view="StockDescription"></div>

                            <div data-view="SocialSharing.Flyout" class="product-details-full-social-sharing"></div>

                            <div class="product-details-full-main-bottom-banner">
                                <div id="banner-summary-bottom" class="product-details-full-banner-summary-bottom"></div>
                            </div>
                        </form>
                    {{else}}
                        <div data-view="GlobalViewsMessageView.WronglyConfigureItem"></div>
                    {{/if}}

                    <div id="banner-details-bottom" class="product-details-full-banner-details-bottom" data-cms-area="item_info_bottom" data-cms-area-filters="page_type"></div>
                </div>
			</div>

		</section>

        <section class="product-details-full-bottom-content">
            <div data-cms-area="product_details_full_cms_area_5" data-cms-area-filters="page_type"></div>
            <div data-cms-area="product_details_full_cms_area_6" data-cms-area-filters="path"></div>
            

			<section class="product-details-full-main-product-information-container" data-view="Product.Information"></section>

            <div data-cms-area="product_details_full_cms_area_7" data-cms-area-filters="path"></div>
            {{!-- 
            <div data-view="ProductReviews.Center"></div>
            --}}
            {{!--
                {{log this}}
                {{log model.item}}
               item url {{itemUrl}}
               model.item.keyMapping_name {{model.item._name}}
               --}}
                <section class="yotpo-section">
                    <div class="yotpo yotpo-main-widget"
                        data-product-id="{{model.item.internalid}}"
                        data-price="{{model.item.pricelevel5}}"
                        data-name="{{model.item._name}}"
                        data-url="{{itemUrl}}">
                    </div>
                </section>


            <div data-cms-area="product_details_full_cms_area_8" data-cms-area-filters="path"></div>

        </section>

       

	</article>
</div>
<div class="product-details-related-correlated">
	<div class="product-details-full-content-related-items">
		<div data-view="Related.Items"></div>
	</div>

	<div class="product-details-full-content-correlated-items">
		<div data-view="Correlated.Items"></div>
	</div>

	<div id="banner-details-bottom" class="content-banner banner-details-bottom" data-cms-area="item_details_banner_bottom" data-cms-area-filters="page_type"></div>
</div>

{{!--
<div id="y-embedded-widget" class="yotpo embedded-widget"
data-appkey="iGHqsJuMcb16OLb2hmnYN2bq6K7KbtjkvpTowrYq"
data-product-id="top_rated_products"
data-layout="basic"
data-width="100"
data-reviews="5"
data-header-text="Top Rated Products"
data-header-background-color="919191"
data-body-background-color="FFFFFF"
data-font-size="18"
data-font-color="FFFFFF">&nbsp;</div>
--}}

<script type="text/javascript" ns-server-execute="T">
    (function e(){var e=document.createElement("script");e.type="text/javascript",e.async=true,e.src="https://staticw2.yotpo.com/iGHqsJuMcb16OLb2hmnYN2bq6K7KbtjkvpTowrYq/widget.js";var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)})();

    setTimeout(function () {

        var api = new Yotpo.API(yotpo);
        api.refreshWidgets();

    }, 1000);
</script>

{{!----
Use the following context variables when customizing this template:

	model (Object)
	model.item (Object)
	model.item.internalid (Number)
	model.item.type (String)
	model.quantity (Number)
	model.options (Array)
	model.options.0 (Object)
	model.options.0.cartOptionId (String)
	model.options.0.itemOptionId (String)
	model.options.0.label (String)
	model.options.0.type (String)
	model.location (String)
	model.fulfillmentChoice (String)
	pageHeader (String)
	itemUrl (String)
	isItemProperlyConfigured (Boolean)
	isPriceEnabled (Boolean)

----}}
