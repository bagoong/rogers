define('StockAvailability.CartDetailed.View', [
    'Cart.Lines.View',
    'Cart.Detailed.View',
    'jQuery'
], function StockAvailabilityCartDetailedView(
    CartLinesView,
    CartDetailedView,
    jQuery
){
    'use strict';

    _.extend(CartLinesView.prototype, {
        getContext: _.wrap(CartLinesView.prototype.getContext, function getContext(fn) {
            var context = fn.apply(this, _.toArray(arguments).slice(1));
            var showStockAvailability = false;
            var item = this.model;

            if (item.get('quantity') > item.get('item').get('quantityavailable')) {
                showStockAvailability = true;
            }
            if (item.get('itemtype') === 'GiftCert') {
                showStockAvailability = false;
            }

            return _.extend(context, {
                showStockAvailability: showStockAvailability,
                stockAvailability: item.get('item').get('quantityavailable')
            });
        })
    });

    _.extend(CartDetailedView.prototype, {
        getContext: _.wrap(CartDetailedView.prototype.getContext, function getContext(fn) {
            var context = fn.apply(this, _.toArray(arguments).slice(1));
            var self = this;
            var lines = this.model.get('lines').models;
            this.isOutOfStock = false;
            this.isGiftCard = false;
            _.each(lines, function eachIitem(item) {
                if (item.get('quantity') > item.get('item').get('quantityavailable') && item.get('itemtype') !== 'GiftCert') self.isOutOfStock = true;
            });
            if (this.isOutOfStock) {
                setTimeout(function sto() {
                    jQuery('#btn-proceed-checkout').hide();
                    jQuery('.cart-summary-btn-paypal-express').hide();
                    if (jQuery('p.cart-summary-button-container-error-message').length === 0) {
                        jQuery('.cart-summary-button-container').append('<p class="cart-summary-button-container-error-message">Please fix the error to proceed.</p>');
                    }
                }, 200);
            } else {
                setTimeout(function settimeout() {
                    jQuery('p.cart-summary-button-container-error-message').remove();
                    jQuery('#btn-proceed-checkout').show();
                    jQuery('.cart-summary-btn-paypal-express').show();
                }, 200);
            }

            return _.extend(context, {
            });

        })
    });

});
