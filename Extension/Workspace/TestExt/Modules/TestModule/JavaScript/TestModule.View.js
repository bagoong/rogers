// @module ESP.TestExt.TestModule
define('ESP.TestExt.TestModule.View'
,	[
	'esp_testext_testmodule.tpl'
	
	,	'ESP.TestExt.TestModule.SS2Model'
	
	,	'Backbone'
    ]
, function (
	esp_testext_testmodule_tpl
	
	,	TestModuleSS2Model
	
	,	Backbone
)
{
    'use strict';

	// @class ESP.TestExt.TestModule.View @extends Backbone.View
	return Backbone.View.extend({

		template: esp_testext_testmodule_tpl

	,	initialize: function (options) {

			/*  Uncomment to test backend communication with an example service
				(you'll need to deploy and activate the extension first)
			*/

			// this.model = new TestModuleModel();
			// var self = this;
         	// this.model.fetch().done(function(result) {
			// 	self.message = result.message;
			// 	self.render();
      		// });
		}

	,	events: {
		}

	,	bindings: {
		}

	, 	childViews: {

		}

		//@method getContext @return ESP.TestExt.TestModule.View.Context
	,	getContext: function getContext()
		{
			//@class ESP.TestExt.TestModule.View.Context
			this.message = this.message || 'Hello World!!'
			return {
				message: this.message
			};
		}
	});
});
