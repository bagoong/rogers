// Model.js
// -----------------------
// @module Case
define("ESP.TestExt.TestModule.SS2Model", ["Backbone", "Utils"], function(
    Backbone,
    Utils
) {
    "use strict";

    // @class Case.Fields.Model @extends Backbone.Model
    return Backbone.Model.extend({
        //@property {String} urlRoot
        urlRoot: Utils.getAbsoluteUrl(
            getExtensionAssetsPath(
                "Modules/TestModule/SuiteScript2/TestModule.Service.ss"
            ),
            true
        )
});
});
