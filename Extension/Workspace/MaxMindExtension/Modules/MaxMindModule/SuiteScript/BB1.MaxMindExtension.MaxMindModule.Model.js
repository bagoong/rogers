// BB1.MaxMindExtension.MaxMindModule.Model.js
// ----------
// Handles integration on MaxMind into the checkout.
define(
	'BB1.MaxMindExtension.MaxMindModule.Model'
,	[	'Application'
	,	'Utils'
	,	'SC.Model'
 
	,	'underscore'
	]
,	function (
		Application
	,	Utils
	,	SCModel

	,	_
	)
{
	'use strict';

	return SCModel.extend({
 
		name: 'MaxMind',

  isSecure: request.getURL().indexOf('https') === 0,

  getHeader: function (header)
  {
   'use strict'
   
   if (!header) return "";
   
   return request.getHeader(header) || request.getHeader(header.toUpperCase()) || "";
  },
  
  setCheckoutDetails: function ()
  {
   'use strict';

   var ipAddress = this.getHeader("NS-Client-IP");

   if (ipAddress != "") {
   
    var userAgent = this.getHeader("User-Agent"),
        acceptLanguage = this.getHeader("Accept-Language"),
        sessionCookie = this.getHeader("Cookie"),
        cookies = sessionCookie.split(";");
        
    _.each(cookies, function (cookie) {
     if (cookie.trim().substr(0, 11) == "JSESSIONID=") {
      sessionCookie = cookie.trim().substr(12);
      return false;
     }
    });
    
    var data = ipAddress+"|"+userAgent+"|"+acceptLanguage+"|"+sessionCookie;//,
        //options = this.getTransactionBodyField();
    context.setSetting("SESSION", "custbody_bb1_mm_webdata", data);
    console.log("custbody_bb1_mm_webdata", data);
    //options.custbody_bb1_mm_webdata = data;
    //this.setTransactionBodyField(options);
   
   }
   
  },
  
  setTransactionBodyField: function(options)
  {
   'use strict';
   
   if (this.isSecure && !_.isEmpty(options))
   {
    order.setCustomFieldValues(options);
   }
  },
  
  getTransactionBodyField: function ()
  {
   'use strict';

   var options = {};

   if (this.isSecure)
   {
    _.each(order.getCustomFieldValues(), function (option)
    {
     options[option.name] = option.value;
    });

   }
   return options;
  }
  
	});
});