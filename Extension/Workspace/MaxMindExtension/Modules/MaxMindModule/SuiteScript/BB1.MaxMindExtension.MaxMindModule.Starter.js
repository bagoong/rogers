// BB1.MaxMindExtension.MaxMindModule.Starter.js
// ----------
// MaxMind Starter

define(
	'BB1.MaxMindExtension.MaxMindModule.Starter'
,	[
		'Application',
		'Configuration',
		'LiveOrder.Model',
		'BB1.MaxMindExtension.MaxMindModule.Model',
  
		'underscore'
	]
,	function(
 Application,
 Configuration,
 LiveOrder,
 MaxMind,
 
		_
	)
{
	'use strict';

 LiveOrder.update = _.wrap(LiveOrder.update, function (originalUpdate, data) {
  'use strict';
   
  var result = originalUpdate.apply(this, _.rest(arguments));
  
  console.log("after:LiveOrder.update");
  
  var is_secure = request.getURL().indexOf('https') === 0;
  
  if (is_secure)
  {
   console.log('MaxMind.setCheckoutDetails()', JSON.stringify(data));
   MaxMind.setCheckoutDetails();
  }
  
  return result;
  
 });
 
});