// @module ACS.PricePerRoundAmmunition.PricePerRoundAmmunition
define('ACS.PricePerRoundAmmunition.PricePerRoundAmmunition.View'
, [
    'ProductViews.Price.View',
    'product_views_price_round.tpl',
    'Utils',
    'Backbone'
]
, function (
    ProductViewsPriceView,
productViewsPriceTpl
, Utils
, Backbone
) {
    'use strict';

// @class ACS.PricePerRoundAmmunition.PricePerRoundAmmunition.View @extends Backbone.View
    return Backbone.View.extend({

        template: productViewsPriceTpl,

        contextDataRequest: ['item'],

        getContext: function getContext() {
            var item = this.contextData.item();
            var roundsPerCase = item.custitemroundsper;
            var hasQuantityPricing = item.onlinecustomerprice_detail.priceschedule && item.onlinecustomerprice_detail.priceschedule.length;
            var qty = hasQuantityPricing && item.onlinecustomerprice_detail.priceschedule.length;
            var maxPriceQty = hasQuantityPricing && item.onlinecustomerprice_detail.priceschedule[0].price;
            var minPriceQty = hasQuantityPricing && item.onlinecustomerprice_detail.priceschedule[qty - 1].price;
            var maxPriceQtyPerRound = hasQuantityPricing && maxPriceQty / roundsPerCase;
            var minPriceQtyPerRound = hasQuantityPricing && minPriceQty / roundsPerCase;
            var pricePerRound = item.onlinecustomerprice_detail.onlinecustomerprice / roundsPerCase;

            SC.item = this.contextData.item();

            return {
            // @property {boolean} showComparePrice
                isAmmunition: item.custitemroundsper > 0,
                hasQuantityPricing: hasQuantityPricing && item.onlinecustomerprice_detail.priceschedule.length > 0,
                minPriceQty: minPriceQty,
                maxPriceQty: maxPriceQty,
                minPriceQtyPerRound: minPriceQtyPerRound && minPriceQtyPerRound.toFixed(2),
                maxPriceQtyPerRound: maxPriceQtyPerRound && maxPriceQtyPerRound.toFixed(2),
                isPriceEnabled: true,
                pricePerRound: pricePerRound && pricePerRound.toFixed(2)

            };

            /* return _.extend(context, {
                isAmmunition: item.custitemroundsper > 0,
                hasQuantityPricing: item.onlinecustomerprice_detail.priceschedule.length > 0,
                minPriceQty: minPriceQty,
                maxPriceQty: maxPriceQty,
                minPriceQtyPerRound: minPriceQtyPerRound,
                maxPriceQtyPerRound: maxPriceQtyPerRound
            }); */
        }
    // )
    });
});
