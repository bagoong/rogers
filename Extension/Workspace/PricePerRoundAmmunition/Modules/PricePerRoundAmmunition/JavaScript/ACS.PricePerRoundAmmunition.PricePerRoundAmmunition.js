define(
'ACS.PricePerRoundAmmunition.PricePerRoundAmmunition'
, [
    'ACS.PricePerRoundAmmunition.PricePerRoundAmmunition.View'
]
, function (
PricePerRoundAmmunitionView
) {
    'use strict';

    return {
        mountToApp: function mountToApp(container) {
            var PLPComponent = container.getComponent('PLP');
            var PDPComponent = container.getComponent('PDP');

            PLPComponent.addChildViews(
PLPComponent.PLP_VIEW
  , {
      'ProductViewsPrice.Price': {
          'ProductViewsPrice.Price.PerRound': {
              childViewIndex: 2,
              childViewConstructor: function () {
                  return new PricePerRoundAmmunitionView({ container: container });
              }
          }
      }
  }
  );

            PDPComponent.addChildViews(
PDPComponent.PDP_FULL_VIEW
  , {
      'Product.Price': {
          'Product.Price.PerRound': {
              childViewIndex: 2,
              childViewConstructor: function () {
                  return new PricePerRoundAmmunitionView({ container: container });
              }
          }
      }
  }
  );

            PDPComponent.addChildViews(
PDPComponent.PDP_QUICK_VIEW
  , {
      'Product.Price': {
          'Product.Price.PerRound': {
              childViewIndex: 2,
              childViewConstructor: function () {
                  return new PricePerRoundAmmunitionView({ container: container });
              }
          }
      }
  }
  );
        }
    };
});
