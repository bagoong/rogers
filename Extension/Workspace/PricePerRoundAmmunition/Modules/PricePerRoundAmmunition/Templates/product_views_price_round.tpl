{{#if isPriceEnabled}}
    {{#if isAmmunition}}
        {{#if hasQuantityPricing}}
            <span class="">
                <!-- Single -->
                <span class="price-per-round-price">
                    (${{minPriceQtyPerRound}} - ${{maxPriceQtyPerRound}} per round)
                </span>
            </span>
        {{else}}
            <span class="">
                <!-- Single -->
                <span class="price-per-round-price">
                    (${{ pricePerRound }} per round)
                </span>
            </span>
        {{/if}}
    {{/if}}
{{/if}}