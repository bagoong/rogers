/**
 * Copyright (c) 1998-2020 NetSuite, Inc.
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */

define('AvailableInventory.View', [
    'AvailableInventory.Helper',
    'Backbone',
    'available_inventory.tpl'
], function AvailableInventory(
    AvailableInventoryHelper,
    Backbone,
    availableInventoryTpl
) {
    'use strict';

    // @class AvailableInventory.View @extends Backbone.View
    return Backbone.View.extend({

        template: availableInventoryTpl,

        contextDataRequest: ['item'],

        getContext: function getContext() {
            SC.item = this.contextData.item();
            return {};
        }
    });
});

