/**
 * Copyright (c) 1998-2020 NetSuite, Inc.
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */

define('AvailableInventory.Helper', [
    'underscore'
], function AvailableInventoryHelper(
    _
) {
    'use strict';

    return {

        /**
         * Returns the quantity the item already in the cart
         * @param {int} cartItemId
         * @param {array} cartlines
         * @returns {int} qtyInCart
         */
        getItemsInCart: function getItemsInCart(cartItemId, cartLines) {
            var qtyInCart = 0;
            _.each(cartLines, function eachLines(line) {
                qtyInCart = line.item.internalid === cartItemId ? qtyInCart + line.quantity : qtyInCart;
            });
            return qtyInCart;
        },

        /**
         * Get the quantityAvailable of the item to be added to the shoppingCart
         * based on Rogers warehouse + Absolute value of(Liberty Retail location - buffer stock)
         * the result of that logic is recalculated based on the items added to the cart
         * @param {itemModel} item
         * @param {cartItemModel} cartItem
         * @returns {int} quanity available of the item
         */
        getItemAvailability: function getItemAvailability(item, cartItem) {
            var quantityAvaialbleDetails;
            var quantityAvaialble = 0;
            var bufferQuantity;
            var childItem;
            var cartItemId;
            cartItemId = cartItem.item.internalid;
            if (item.matrixchilditems_detail && item.matrixchilditems_detail.length > 0) {
                childItem = _.find(item.matrixchilditems_detail, function findChild(child) {
                    return child.internalid === cartItemId;
                });
                quantityAvaialbleDetails = childItem.quantityavailable_detail;
                bufferQuantity = childItem.custitem_acs_buffer_stock;
            } else {
                quantityAvaialbleDetails = item.quantityavailable_detail;
                bufferQuantity = item.custitem_acs_buffer_stock;
            }

            bufferQuantity = parseInt(bufferQuantity, 0);
            bufferQuantity = isNaN(bufferQuantity) ? 0 : bufferQuantity;

            // todo avoid substract buffer if no qty avail on liverty retail
            _.each(quantityAvaialbleDetails.locations, function eachLocation(location) {
                if (cartItem.extras && cartItem.extras.location && cartItem.extras.fulfillmentChoice === 'pickup') {
                    if (location.internalid === 1 && location.quantityavailable > 0 && parseInt(cartItem.extras.location, 0) === parseInt(location.internalid, 0)) {
                        quantityAvaialble += (location.quantityavailable - bufferQuantity);
                    } else if (cartItem.extras.location === location.internalid) {
                        quantityAvaialble += location.quantityavailable;
                    }
                } else if (location.internalid === 1 && location.quantityavailable > 0) {
                    quantityAvaialble += (location.quantityavailable - bufferQuantity);
                } else {
                    quantityAvaialble += location.quantityavailable;
                }
            });

            return quantityAvaialble;
        },

        /**
         * Evaulates if an item is available to be added to the shopping Cart
         * @param {itemModel} item
         * @param {cartItemModel} cartItem
         * @param {array} cartlines
         * @returns {boolean}
         */
        confirmAvailability: function confirmAvailability(item, cartItem, cartlines, fromCart) {
            var quantityToAdd;
            var quantityAvaialble = 0;
            var itemAvaialble = 0;
            var isAvaialble = true;
            var errorMessage;
            var qtyInCart;
            try {
                quantityToAdd = cartItem.quantity;
                quantityAvaialble = this.getItemAvailability(item, cartItem, cartlines);
                itemAvaialble = quantityAvaialble;
                if (!fromCart) {
                    // calculate the qty based on items already in cart
                    qtyInCart = this.getItemsInCart(cartItem.item.internalid, cartlines);
                    quantityAvaialble -= qtyInCart;
                }

                isAvaialble = !item.isbackorderable && quantityToAdd <= quantityAvaialble;
            } catch (e) {
                console.log(e);
            }
            if (!isAvaialble) {
                itemAvaialble = itemAvaialble < 0 ? 0 : itemAvaialble;
                errorMessage = 'Sorry, You can`t have more than ' + itemAvaialble + ' of ' + item.storedisplayname2 + ' in the Shopping Cart.';
                throw {
                    responseJSON: {
                        'errorCode': 'ERR_EXT_CANCELED_OPERATION',
                        'error': errorMessage,
                        'errorMessage': errorMessage,
                        'errorDetails': errorMessage
                    },
                    // eslint-disable-next-line max-len
                    responseText: '{"errorCode":"ERR_EXT_CANCELED_OPERATION","error":"' + errorMessage + '","errorMessage":"' + errorMessage + '","errorDetails":"' + errorMessage + '"}'
                };
            }
        }
    };
});
