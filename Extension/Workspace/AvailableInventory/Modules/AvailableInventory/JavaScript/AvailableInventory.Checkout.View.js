// @module ACS.OutOfStockMessage.OutOfStockMessage
define('AvailableInventory.Checkout.View', [
    'AvailableInventory.Helper',
    'available_inventory.tpl',
    'Backbone',
    'underscore'
], function AvailableInventoryCartView(
    AvailableInventoryHelper,
    availableInventoryTpl,
    Backbone,
    _
) {
    'use strict';

    // @class ACS.OutOfStockMessage.OutOfStockMessage.View @extends Backbone.View
    return Backbone.View.extend({

        template: availableInventoryTpl,

        getItemToUpdate: function getItemToUpdate(cartItem) {
            var item;
            item = _.find(this.cartLines, function eachLines(line) {
                return line.item.internalid === cartItem.line.item.internalid;
            });
            return item;
        },

        initialize: function initialize(options) {
            var self = this;
            var cart = options.container.getComponent('Cart');
            this.cartComponent = cart;
            this.cartLines = [];
            if (cart) {
                cart.getLines().done(function getLines(lines) {
                    self.cartLines = lines;
                });
                cart.on('beforeSubmit', function beforeUpdateLine() {
                    _.each(self.cartLines, function each(line) {
                        AvailableInventoryHelper.confirmAvailability(line.item.extras, line, self.cartLines, true);
                    });
                });
            }
        }
    });
});
