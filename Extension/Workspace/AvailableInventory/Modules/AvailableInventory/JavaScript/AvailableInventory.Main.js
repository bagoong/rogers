/* eslint-disable linebreak-style */
/**
 * Copyright (c) 1998-2020 NetSuite, Inc.
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */

// @module AvailableInventory
define('AvailableInventory.Main', [
    'AvailableInventory.View',
    'AvailableInventory.Cart.View',
    'AvailableInventory.Helper'
], function AvailableInventory(
    AvailableInventoryView,
    AvailableInventoryCartView,
    AvailableInventoryHelper
) {
    'use strict';

    return {
        mountToApp: function mountToApp(container) {
            // var layout = container.getComponent('Layout');
            var pdp = container.getComponent('PDP');
            var plp = container.getComponent('PLP');
            var cart = container.getComponent('Cart');
            var self = this;
            this.cartLines = [];
            this.cartComponent = cart;
            if (pdp) {
                pdp.addChildView('MainActionView', function NewAccountRequest() {
                    return new AvailableInventoryView({
                        container: container
                    });
                });
            }

            if (plp) {
                plp.addChildView('AddToCart', function NewAccountRequest() {
                    return new AvailableInventoryView({
                        container: container
                    });
                });
            }

            if (cart) {
                cart.addChildView('Item.Summary.View', function NewAccountRequest() {
                    return new AvailableInventoryCartView({
                        container: container
                    });
                });
                cart.getLines().done(function getLines(lines) {
                    self.cartLines = lines;
                });
                cart.on('beforeAddLine', function beforeAddLine(cartItem) {
                    if (SC.item && SC.item.internalid === cartItem.line.item.internalid) {
                        AvailableInventoryHelper.confirmAvailability(SC.item, cartItem.line, self.cartLines);
                    }
                });
                cart.on('afterAddLine', function afterAddLine() {
                    self.cartComponent.getLines().done(function getLines(lines) {
                        self.cartLines = lines;
                    });
                });
                cart.on('afterUpdateLine', function afterAddLine() {
                    self.cartComponent.getLines().done(function getLines(lines) {
                        self.cartLines = lines;
                    });
                });
            }
        }
    };
});
