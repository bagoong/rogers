/**
 * Copyright (c) 1998-2020 NetSuite, Inc.
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */

// @module AvailableInventory
define('AvailableInventory.Checkout.Main', [
    'AvailableInventory.Checkout.View'
], function AvailableInventory(
    AvailableInventoryCheckoutView
) {
    'use strict';

    return {
        mountToApp: function mountToApp(container) {
            // var layout = container.getComponent('Layout');
            var checkout = container.getComponent('Checkout');

            if (checkout) {
                checkout.addChildView('Cart.PromocodeForm', function NewAccountRequest() {
                    return new AvailableInventoryCheckoutView({
                        container: container
                    });
                });
            }
        }
    };
});
