define('RedeemLoyaltyPoints.View'
, [
      'Wizard.Module'
    , 'GlobalViews.Message.View'
    , 'Profile.Model'
    , 'LiveOrder.Model'
    , 'Utils'

    , 'redeem_loyalty_points.tpl'
  ]
, function (
    WizardModule
  , GlobalViewsMessageView
  , ProfileModel
  , LiveOrderModel
  , Utils

  , redeem_loyalty_points_tpl
  )
{
  'use strict';

  return WizardModule.extend({
    profile: ProfileModel.getInstance()
  , order: LiveOrderModel.getInstance()

  , template: redeem_loyalty_points_tpl
  , events: {
    'submit .cart-loyaltypoints-form': 'redeemLoyaltyPoints'
    , 'click [data-action="remove-loyalty-points"]': 'removeLoyaltyPoints'
    , 'change .cart-loyaltypoints-form-summary-input': 'changeRedeemPointsInput'
  }

  , removeLoyaltyPoints: function(e){
    e.preventDefault();

    var dataInput = jQuery(e.currentTarget).serializeObject()
      , self = this
      , totalPointsRedeemed = 0;

      this.model.set('options', _.extend(this.model.get('options'), {"custbody_rogers_points_redeemed": totalPointsRedeemed.toString()}));
      this.model.save().then(function(){ self.render(); });
  }
  , changeRedeemPointsInput: function(e){
    var $el = jQuery(e.currentTarget);
    if($el.val() != 0) this.$('.cart-loyaltypoints-form-summary-button-apply-loyaltypoints').attr('disabled', false);
    else this.$('.cart-loyaltypoints-form-summary-button-apply-loyaltypoints').attr('disabled', true);
  }
  , redeemLoyaltyPoints: function(e){
    e.preventDefault();
    var dataInput = jQuery(e.currentTarget).serializeObject(),
      messageView = {}
      self = this;
   // debugger;
    if(this.validateForm(dataInput)){
      //this.model.save();
        var messageView = new GlobalViewsMessageView({
          message: _.translate('Redeeming points...')
        ,	type: 'success'
        ,	closable: false
        });

        dataInput.loyaltypoints = dataInput.loyaltypoints * 50;
        console.log('dataInput.loyaltypoints', dataInput.loyaltypoints)

        var points_redeemed = this.order.get('options').custbody_rogers_points_redeemed ? parseInt(this.order.get('options').custbody_rogers_points_redeemed) + parseInt(dataInput.loyaltypoints) : parseInt(dataInput.loyaltypoints);
        this.model.set('options', _.extend(this.model.get('options'), {"custbody_rogers_points_redeemed": points_redeemed.toString()}));
        this.model.save().then(function(){ self.render(); });
        //console.log('redeem loyaltu', this.model)        
    }
    else{
      var messageView = new GlobalViewsMessageView({
        message: _.translate('You can\'t redeem more than your available points.')
      ,	type: 'error'
      ,	closable: false
      });
      
    }
    this.$('.order-wizard-loyaltypoints-message').html(messageView.render().$el);
  }

  , render: function(){
    this._render();
  }

  , validateForm: function(dataInput){
    var loyaltypoints = dataInput.loyaltypoints
    , loyalty_points_balance = this.profile.get('custentity_rogers_loyalty_points_balance') - this.order.get('options').custbody_rogers_points_redeemed;

    if(loyaltypoints <= 0)
      return false;
    if(loyaltypoints != '' && loyaltypoints <= loyalty_points_balance) return true;
    else return false;
  }

  , getContext: function getContext()
    {
      console.log('this.model', this.model, this.profile)
     // debugger;
      //var loyalty_points_balance = this.profile.get('custentity_rogers_loyalty_points_balance') - this.order.get('options').custbody_rogers_points_redeemed;


      //TODO
      var testl = parseInt((Math.floor((this.profile.get('custentity_rogers_loyalty_points_balance')))));

      var test2 = parseInt((this.order.get('options').custbody_rogers_points_redeemed / 250).toFixed() * 5);

      var test3 = this.order.get('options').custbody_rogers_points_redeemed; 
      var options = this.order.get('options');
      var order = this.order;

      console.log({testl});
      console.log({test2});
      console.log({test3});
      console.log({options});
      console.log({order});

//       options:
// custbody_license_number: ""
// custbody_rogers_points_redeemed: ""


      var loyalty_points_balance = parseInt((Math.floor((this.profile.get('custentity_rogers_loyalty_points_balance') - this.order.get('options').custbody_rogers_points_redeemed) / 250)).toFixed() * 5);
      
      //TODELETE
      console.log({loyalty_points_balance});
      return {
        isReview: this.step.step_url == 'review'
        //, loyalty_points_balance: this.profile.get('custentity_rogers_loyalty_points_balance')
          , loyalty_points_balance: Utils.formatCurrency(loyalty_points_balance)
          , loyalty_points_balance_max: loyalty_points_balance
          , pointsRedeemed: this.order.get('options').custbody_rogers_points_redeemed > 0
          , showButton: !(this.order.get('options').custbody_rogers_points_redeemed >= this.profile.get('custentity_rogers_loyalty_points_balance'))
      };
    }
  });
});