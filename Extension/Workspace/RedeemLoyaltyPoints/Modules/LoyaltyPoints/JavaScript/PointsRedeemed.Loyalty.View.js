define('PointsRedeemed.Loyalty.View'
,   [
    'Backbone'

    ,   'redeemed_loyalty_points.tpl'
]
,   function(
    Backbone

    ,   redeemed_loyalty_points_tpl
){
    'use strict';

    return Backbone.View.extend({
        template: redeemed_loyalty_points_tpl  

        ,   initialize: function(options){
          //  console.log('loyalty points redeemed', options)
            this.model = options.item;
            this.model.on('points-redeemed', this.render, this);
        }
        ,   isEnabled: function(){
            if(this.model.get('options').custbody_rogers_points_redeemed > 0) return true
            else return false;
        }
        ,   getContext: function () {
			return {
				showLoyaltyPoints: this.isEnabled()
			};
		}
    });
})