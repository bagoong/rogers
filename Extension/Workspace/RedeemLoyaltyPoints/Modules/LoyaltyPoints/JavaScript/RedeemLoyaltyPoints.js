define('RedeemLoyaltyPoints'
, [
    'RedeemLoyaltyPoints.View'
    , 'OrderWizard.Module.CartSummary'
    ,   'PointsRedeemed.Loyalty.View'

    ,	'OrderWizard.Module.Shipmethod'
	  ,	'OrderWizard.Module.ShowShipments'

    , 'Utils'
  ]
,   function
  (
    RedeemLoyaltyPointsView
    ,   OrderWizardModuleCartSummary
    ,   PointsRedeemedLoyaltyView

    ,	OrderWizardModuleShipmethod
	  ,	OrderWizardModuleShowShipments

    , Utils
  )
{
  'use strict';

  _.extend(OrderWizardModuleShipmethod.prototype, {
		getContext: _.wrap(OrderWizardModuleShipmethod.prototype.getContext, function(fn){
			var context = fn.apply(this, _.toArray(arguments).slice(1));
			var self = this;
			var shipping_methods = this.model.get('shipmethods').map(function (shipmethod)
			{
				return {
						name: shipmethod.get('name')
					,	rate_formatted: shipmethod.get('rate_formatted')
					,	internalid: shipmethod.get('internalid')
					,	isActive: shipmethod.get('internalid') === self.model.get('shipmethod')
				};
			});

			_.each(this.model.get('lines').models, function(lines){
				if(lines.get('item').get('custitem_donotshipair'))
					if(_.findWhere(shipping_methods, {internalid: "7033"})) _.findWhere(shipping_methods, {internalid: "7033"}).hideIt = true;
			});

			return _.extend( context, {
				shippingMethods: shipping_methods
			});
		})
	});

	_.extend(OrderWizardModuleShowShipments.prototype, {
		getContext: _.wrap(OrderWizardModuleShowShipments.prototype.getContext, function(fn){
		//	debugger;
			var context = fn.apply(this, _.toArray(arguments).slice(1));
			var self = this;
			var shipping_methods = this.model.get('shipmethods').map(function (shipmethod)
			{
				return {
						name: shipmethod.get('name')
					,	rate_formatted: shipmethod.get('rate_formatted')
					,	internalid: shipmethod.get('internalid')
					,	isActive: shipmethod.get('internalid') === self.model.get('shipmethod')
				};
			});

			_.each(this.model.get('lines').models, function(lines){
				if(lines.get('item').get('custitem_donotshipair'))
					if(_.findWhere(shipping_methods, {internalid: "7033"})) _.findWhere(shipping_methods, {internalid: "7033"}).hideIt = true;
			});

			return _.extend( context, {
				shippingMethods: shipping_methods
			});
		})
	});

  _.extend(OrderWizardModuleCartSummary.prototype, {
      initialize: _.wrap(OrderWizardModuleCartSummary.prototype.initialize, function(fn){
        fn.apply(this, _.toArray(arguments).slice(1));
        this.model.on('points-redeemed', this.recalcSubtotal, this);
      })

      , childViews: _.extend(OrderWizardModuleCartSummary.prototype.childViews, {
        'CartLoyaltyPointsListView': function ()
        {
         // console.log(this)
          return new PointsRedeemedLoyaltyView({
              item: this.model || []
          });
        }
      })
      , recalcSubtotal: function(){
        this.model.get('summary').total_formatted = Utils.formatCurrency(this.model.get('summary').total - 10);
        //this.model.get('subtotal_formatted')
        this.render();
      }
  });

  return  {
    mountToApp: function mountToApp (container)
    {
      var checkout = container.getComponent('Checkout');

      if(checkout)
        checkout.addModuleToStep(
        {
          step_url: 'shipping/address'
        , module: {
            id: 'RedeemLoyaltyPointsView'
          , index: 6
          , classname: 'RedeemLoyaltyPoints.View'
          , options: { container: '#wizard-step-content-right'}
          }
        });
    }
  };
});