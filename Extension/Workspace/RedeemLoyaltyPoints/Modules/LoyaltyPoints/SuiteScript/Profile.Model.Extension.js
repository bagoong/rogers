
define(
    'Profile.Model.Extension'
    , ['SC.Models.Init',
        'Utils',
        'underscore',
        'Profile.Model']

    , function (ModelsInit,
                Utils,
                _,
                ProfileModel) {
        'use strict';

        return _.extend(ProfileModel, {

            get: _.wrap(ProfileModel.get, function (fn) {

                try{
                    nlapiLogExecution('ERROR','profile model extension', ModelsInit.session.isLoggedIn2());
                    var ret = fn.apply(this, _.toArray(arguments).slice(1));

                    if (ModelsInit.session.isLoggedIn2() && this.isSecure)
			        {
                        var customFields = ModelsInit.customer.getCustomFieldValues();
                        for (var i in customFields) {
                            var obj = customFields[i];
                            if (obj.name === "custentity_rogers_loyalty_points_balance") {
                                ret["custentity_rogers_loyalty_points_balance"] = obj.value
                            }
                        }
                    }

                    nlapiLogExecution('ERROR','profile model extension ret', JSON.stringify(ret));
                    return ret;
                }
                catch(e){
                    nlapiLogExecution('ERROR','error in profile model',JSON.stringify(e));
                }
            })
        });
    });
