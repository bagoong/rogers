{{!-- Edited for Manor Theme --}}
<section class="facets-browse-category-heading-list-header  {{#if hasBanner}}facets-browse-category-heading-list-header-with-image{{/if}}">
	
		<div class="facets-browse-category-heading-main-description">
			<h1>{{pageheading}}</h1>
		</div>
		<div class="col-md-3">
		{{#if hasBanner}}
			<div class="facets-browse-category-heading-main-image">
				<img src="{{banner}}" alt="{{pageheading}}"/>
			</div>
		{{/if}}
		</div>
	<div class="col-md-9">
		<div class="facet_browse_category_heading01" data-cms-area="facet_browse_category_heading01" data-cms-area-filters="path"></div>
	</div>
</section>




{{!----
Use the following context variables when customizing this template: 
	
	name (String)
	banner (String)
	description (String)
	pageheading (String)
	hasBanner (Boolean)
	showDescription (Boolean)

----}}
